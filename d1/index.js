// console.log("Hello World");

//Functions
	/*
		Parameters ang Arguments
	*/

	function printInput(){
		let nickName = prompt("Enter your nickname: ");
		console.log("Hi, " + nickName);
	};
	// printInput();

	//However, for other cases, function can also process data directly passed into it instead of relying only on global variable and prompt()
	function printName(name){
		console.log("My name is " + name);
	};
	printName("Juana");

	/*
	- you can directly pass data into the function.
	- the function can then call or use that data which is referred as  "name"  within the function
	- "name" is called a parameter.
	- a "parameter" acts as a named variable or container that exist only inside of a function.
	- it is used to store information that is provided to a function when it is called or invoked.

	- "juana", the information or data provided directly into a function is called an argument.
	*/

	//in the following examples, "john" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message.

	printName("John");
	printName("Jane");
	//everytime na nag iinvoke nag pprint ng bago with the value in that argument.


	//variables can also be passed as an arguments
	let sampleVariable = "Yui";
	printName(sampleVariable);

	//Function arguments cannot be used by a function if there are no parameters provided within the function.


	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + "divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is" + num + "divisible by 8?");
		console.log(isDivisibleBy8);
	};
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


	//Mini Activity
	function checkDivisibilityBy4(num) {
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	};
	checkDivisibilityBy8(56);
	checkDivisibilityBy8(95);

	//you can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.



//Functions as Arguments
	/*
		- Function parameters can also accept other functions as arguments.
		- some complex functions use other functions as arguments to perform more complicated results.
		- This will be further seen when we discuss array methods.
	*/

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.")
	};

	function invokeFunction(argumentFunction) {
		argumentFunction();
	};
	invokeFunction(argumentFunction);

	//adding and removing the parentheses "()" impacts the ourput of JavaScript heavily.
	//when a function is used with parentheses "()" it denotes invoking or calling a function.
	//a function used without a parenthesis is normally associated with using the function as an argument to another function.

	console.log(argumentFunction);
	//for finding more information about a function in the console using console.log()



//Multiple Parameters
	/*
		multiple "arguments" will correspond to the number of "parameters" declared in a function
	*/
	function createFullName(firstName, middleName, lastName) {
		console.log(firstName + " " + middleName + " " + lastName);
	};
	createFullName("Juan", "Dela", "Cruz");

	//in JS, providing more or less arguments than the expected parameters will NOT return an error.
	createFullName("juan", "dela");
	createFullName("jane", "dela", "cruz", "hello");

	//we can use variables as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);


	function printFullName(middleName, firstName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	};
	printFullName("juan", "dela", "cruz");
	//results to "dela juan cruz" because "juan" was recieved as middleName, "dela" was received as firstName



	//Mini Activity
	/*create a function called printFriends
		3 parameters
		my three friends are:
	*/
	function printFriend(friend1, friend2, friend3) {
		console.log("My three friends are: " + " " +friend1 +" " + friend2 +" " + friend3);
	};
	printFriend("Tolits", "Coco", "Martin");



//Return Statement
	/*
		- The "return" statement allows us to output a value from a function to be passed  to the line or block of code that invoked or called the function.
	*/

	function returnFullName(firstName, middleName, lastName) {
		// console.log(firstName+" "+middleName+" "+lastName);
		return firstName + " " + middleName + " " + lastName;
		console.log("This message will not be printed.");
		//notice that the console.log after the return in no longer printed in the console...
		//that is because ideally any line or block of code that comes after the return statement is ignore because it ends the function execution.
	};
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName);

	//in our example, the "return" statement allows us to output a value of a function to be passed to a line or block of code that called the function.
	//The "returnFullName" function was called in the same line as declaring a variable.
	//whatever value is returned from "returnFullName" function is stored in the "completeName" variable. 
	//This way a function is able to return a value we can further use or manipulate in our program instead of only printing or displaying it in the console.

	console.log(returnFullName(firstName, middleName, lastName));
	//in this example, console.log() will print the return value of the returnFullName().


	function returnAddress(city, country){
		let fullAddress = city+", "+country;
		return fullAddress
	};
	let myAddress = returnAddress("Taguig City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		// console.log("username: "+username);
		// console.log("level: "+level);
		// console.log("job: "+job);
		return("username: "+username+" level: "+level+" job: "+job);
	};
	let user1 = printPlayerInfo("knight_white", 95, "Paladin");
	console.log(user1);


	/*
		Create a function that will be able to multiply two numbers
		**numbers must be provided as arguments
		**return the result of the multiplication

		create a global variable outside of the cuntion called "product"
			-this product variable should be able to receive and store the result of the multiplication function.
			-log the value of the product variable in the console.
	*/



	//Mini Activity
		function multiplication(num1, num2) {
			return num1 * num2;
		};
		console.log("The product of 5 and 4")
		let product = multiplication(5, 4);
		console.log(product);